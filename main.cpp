// ImGui - standalone example application for SDL2 + OpenGL
// If you are new to ImGui, see examples/README.txt and documentation at the top
// of imgui.cpp.
// (SDL is a cross-platform general purpose library for handling windows,
// inputs, OpenGL/Vulkan graphics context creation, etc.)
// (GL3W is a helper library to access OpenGL functions since there is no
// standard header to access modern OpenGL functions easily. Alternatives are
// GLEW, Glad, etc.)

#include "imgui.h"
#include "imgui_impl_sdl_gl3.h"
#include <stdio.h>
#include <vector>
#include <GL/gl3w.h>  
#include <SDL.h>
#include <iostream>


#include <iostream>
#include <cmath>
// #include "ptlist.h"

using namespace std;

/*
Given the Square class, add four derived classes so that each moves in a direction.
*/

class Rectangle {
public:
  Rectangle();
  Rectangle(const ImVec2 &p1, const ImVec2 &p2) : pa(p1), pb(p2) {}
  void move() {
    pa.x = pa.x + 0.5;
    pb.x = pb.x + 0.5;    
  }
  void draw(ImDrawList *idl);
  ImVec2 pa, pb;
private:
  
};



class RightRectangle : public Rectangle {
public:
  RightRectangle(const ImVec2 &p1, const ImVec2 &p2) : Rectangle(p1,p2) {}
  void move() {
    pa.x = pa.x + 0.5;
    pb.x = pb.x + 0.5;
  }
};


class LeftRectangle : public Rectangle {
public:
  LeftRectangle(const ImVec2 &p1, const ImVec2 &p2) : Rectangle(p1,p2) {}
  void move() {
    pa.x = pa.x - 0.5;
    pb.x = pb.x - 0.5;
  }
};


class gui {

public:
int show() {
  // Setup SDL
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
    printf("Error: %s\n", SDL_GetError());
    return -1;
  }

  // Setup window
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,
                      SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
  SDL_DisplayMode current;
  SDL_GetCurrentDisplayMode(0, &current);
  SDL_Window* window =
      SDL_CreateWindow("Polygon Viewer", SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, 1280, 720,
                       SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  SDL_GL_SetSwapInterval(1);  // Enable vsync
  gl3wInit();

  // Setup Dear ImGui binding
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;
  // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard
  // Controls
  ImGui_ImplSdlGL3_Init(window);

  // Setup style
  ImGui::StyleColorsDark();

  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);


  // Main loop
  bool done = false;
  while (!done) {

    // If you want to retain your sanity do not read these GUI statements
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      ImGui_ImplSdlGL3_ProcessEvent(&event);
      if (event.type == SDL_QUIT) done = true;
      if (event.type == SDL_KEYDOWN and event.key.keysym.sym == SDLK_ESCAPE) done = true;
    }
    ImGui_ImplSdlGL3_NewFrame(window);

    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    ImU32 col32 = ImColor(ImVec4(1.0f, 1.0f, 0.4f, 1.0f));


    // add each rectangle in the vector rectangles to the drawinglist
    for (auto r: rectangles) {
      r->move();
      draw_list->AddRect(r->pa, r->pb, col32, 0.0f,
                       ImDrawCornerFlags_All, 1.0f);
    }


    // Rendering
    glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
    glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui::Render();
    ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
    SDL_GL_SwapWindow(window);
  }


  // Cleanup
  ImGui_ImplSdlGL3_Shutdown();
  ImGui::DestroyContext();

  SDL_GL_DeleteContext(gl_context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

void addRectangle(Rectangle *rect) { rectangles.push_back(rect); }

private:
  vector <Rectangle *> rectangles;
};





int main(int, char**) { 
  gui myGUI;

  Rectangle rect1(ImVec2(100,100),ImVec2(150,250));
  Rectangle rect2(ImVec2(200,300),ImVec2(250,350));

  myGUI.addRectangle(&rect1);
  myGUI.addRectangle(&rect2);
  myGUI.show(); 
}