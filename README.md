PolyRectangles for the polymorphism hierarchy lab.

Hay que sintall libsdl2-dev para que compile.

El código para este laboratorio está disponible en: 

El programa incluido en `main.cpp` crea una ventana donde muestra unos rectángulos moviendose hacia la derecha. Compílalo y córrelo (en linux) para validar que funciona.

La forma en que crea los rectángulos es la siguiente: 

```cpp
int main(int, char**) { 
  gui myGUI;  

  Rectangle rect1(ImVec2(100,100),ImVec2(150,250));
  Rectangle rect2(ImVec2(200,300),ImVec2(250,350));

  myGUI.addRectangle(&rect1);
  myGUI.addRectangle(&rect2);

  myGUI.show(); 
}
```

* `gui myGUI` - crea un objeto clase gui al que añadiremos los rectangulos para que salgan pintados
* creamos dos objetos (`rect1` y `rect2`) de clase `Rectangle` especificando las coordenadas de la esquina superior izquierda e inferior derecha.
* pasamos las direcciones de esos dos objetos al GUI usando la función `addRectangle()`.
* invocamos a `show()` para que el GUI pinte los rectángulos.

Una vez invocado `show()` el GUI toma control del programa y se queda en un loop donde en cada vuelta invoca la función `move()` de cada rectángulo y lo añade a la lista de cosas a dibujar ( `draw_list->AddRect`). Puedes ver ese loop dentro de la función `show()`.

```cpp
    for (auto r: rectangles) {
      r->move();
      draw_list->AddRect(r->pa, r->pb, col32, 0.0f, ImDrawCornerFlags_All, 1.0f);
    }
```

La razón por la que los rectángulos se mueven hacia la derecha es porque la función `move()` incrementa la coordenada `x` de las esquinas del rectángulo.

### Ejercicio

1. Convierte la función `move()` de la clase `Rectangle` a una **puramente virtual**. 

1. Crea cuatro clases derivadas de la clase `Rectangle`:

	* `LeftRectangle` - se mueve hacia la izquierda
	* `RightRectangle` - se mueve hacia la derecha
	* `UpRectangle` - se mueve hacia  arriba
	* `DownRectangle` - se mueve hacia abajo

1. Crea al menos un objeto de cada una de las nuevas clases y añadelos al GUI (usando `addRectangle()`). 


<iframe width="560" height="315" src="https://www.youtube.com/embed/30k-zeSiQP0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>